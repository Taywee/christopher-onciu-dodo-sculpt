# Christopher Onciu Dodo Sculpt

An attempt at an educational sculpt of Chritopher Onciu's dodo concept here: https://christopheronciu.blogspot.com/2013/07/creature-design.html

# Copyright

Original design copyright 2013 Christopher Onciu.

My sculpt copyright 2021 Taylor C Richberger, released under the Creative
Commons BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/

A copy of this license is included in the LICENSE file in this repository.
